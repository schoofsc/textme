<?php
//aktuelle session zerstören
session_start();
session_destroy();

//db.php einfügen
include("./php/db.php");

//neue Klasse Database einbinden
$Database = new Database();

//Datenbank Funktion logout aufrufen
$Database->logout();

//zur Login / Registierungsseite wechseln
header('Location: ./index.php');

?>