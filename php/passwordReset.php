<?php

//db.php einfügen
include("./../php/db.php");

//neue Klasse Database einbinden
$Database = new Database();

/**
 * use Namespace
 */
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

/**
 * "Einfügen" der folgdenden Dateien
 */
require 'mail/src/Exception.php';
require 'mail/src/PHPMailer.php';
require 'mail/src/SMTP.php';

/**
 * Ruft die Funktion generatePassword auf und schreibt den Rückgabewert in die Variabel rein
 */
$generatedPassword = generatePassword();

if (isset($_POST) & !empty($_POST)) {
    $mail = $_POST['email'];
    $userExists = $Database->userExists("", $mail);
    if ($userExists == 1) {
        if ($Database->changePasswordByEmail($mail, $generatedPassword)) {
            //echo "<div class='alert alert-info'>PW:[" . $generatedPassword . "]</div>";
            sendPasswordByMail($mail, $generatedPassword);
        }
    } else {
        echo "<div  class='alert alert-danger'>E-Mail Adresse existiert in der Datenbank nicht!</div>";
    }
}

/**
 * Sendet das neue generierte Password per Mail an userf
 * @param $to
 * @param $password
 * @throws Exception
 */
function sendPasswordByMail($to, $password)
{
    $mail = new PHPMailer(true);
    //Tell PHPMailer to use SMTP
    $mail->isSMTP();
    // 0 = off (for production use)
    // 1 = client messages
    // 2 = client and server messages
    $mail->SMTPDebug = 0;
    //Set the hostname of the mail server
    $mail->Host = 'smtp.gmail.com';
    $mail->Port = 587;
    //Set the encryption system to use - ssl (deprecated) or tls
    $mail->SMTPSecure = 'tls';
    //Whether to use SMTP authentication
    $mail->SMTPAuth = true;
    //Username to use for SMTP authentication - use full email address for gmail
    $mail->Username = "peterpandreamworld666@gmail.com";
    //Password to use for SMTP authentication
    $mail->Password = "Pineapple#1";
    //Set who the message is to be sent from
    $mail->setFrom('peterpandreamworld666@gmail.com', 'TextMe');
    //Set who the message is to be sent to
    $mail->addAddress($to, '');
    //Set the subject line
    $mail->Subject = 'Ihr wiederhergestelltes TextMe-Passwort';
    $mail->Body = 'Bitte benutzen Sie dieses Passwort um sich einzuloggen ['. $password .']';
    //send the message, check for errors
    if (!$mail->send()) {
        echo "<div class='alert alert-danger'>Fehler beim Wiederherstellen deines Passworts, versuche es erneut..</div>";
    } else {
        echo "<div class='alert alert-info'>Ihr Passwort wurde an Ihre E-Mail-Adresse gesendet</div>";
    }

}

/**
 * erzeugt Password mit der Länge 8
 * @param int $length
 * @return string
 */
function generatePassword($length = 8)
{
    //mögliche Zeichen die verwendet werden können
    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!§$%&?';
    //anzahl der zeichen in $chars
    $count = mb_strlen($chars);

    //erstellt zufalls Password der länge 8
    for ($i = 0, $result = ''; $i < $length; $i++) {
        $index = rand(0, $count - 1);
        $result .= mb_substr($chars, $index, 1);
    }
    return $result;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!--Browser / Tab - Title einstellen-->
        <title>TextMe - Passwort vergessen..</title>

        <!--Meta Informationen der Website-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="UTF-8">

        <!--Stylesheets einbinden-->
        <link rel="stylesheet" href="../css/bootstrap.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="../css/password-reset.css">

        <!--Stylesheets einbinden-->
        <link rel="icon" href="./../images/Speach-BubbleDialog-512.png">

    </head>
    <body>

        <!-- noscript Funktion -
        Fängt User ab die JavaScript deaktivert haben
        weißt auf eine Anleitung hin um JavaScript wieder zu aktivieren
        -->
        <noscript>
            <div id="nojavascript">
                Diese Anwendung benötitgt JavaScript zum ordungsgemäßen Betrieb.
                Bitte <a href="https://www.enable-javascript.com/" target="_blank" rel="noreferrer"> aktivieren Sie Java
                    Script</a>
                und laden Sie die Seite neu.
            </div>
        </noscript>

        <div class="form-gap"></div>
        <div class="container">
            <div class="row">
                <div class="col-sm-4 offset-sm-4 col-md-4 offset-md-4 col-lg-4 offset-lg-4 col-xl-4 offset-xl-4">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="text-center">
                                <h3><i class="fa fa-lock fa-4x"></i></h3>
                                <h2 class="text-center">Password?</h2>
                                <p>Sie können hier Ihr Passwort zurücksetzen</p>
                                <div class="panel-body">
                                    <form id="register-form" role="form" autocomplete="off" class="form" method="post">

                                        <!-- Email Imput feld -->
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
                                                <input id="email" name="email" placeholder="E-Mail-Adresse" class="form-control" type="email">
                                            </div>
                                        </div>

                                        <!-- Submit Button -->
                                        <div class="form-group">
                                            <input name="recover-submit" class="btn btn-lg btn-primary btn-block"
                                                   value="Zurücksetzen" type="submit">
                                        </div>

                                        <!-- Link zurück zur login Seite -->
                                        <div class="text-center">
                                            <a href="../../" tabindex="5" class="return-to-mainpage">Zurück zur Hauptseite</a>
                                        </div>

                                        <input type="hidden" class="hide" name="token" id="token" value="">
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Java Script Dateien einbinden -->
        <script src='../js/vendor/bootstrap-4.0.0-dist/js/bootstrap.min.js'></script>
        <script src='../js/vendor/jquery-3.3.1.js'></script>
    </body>
</html>