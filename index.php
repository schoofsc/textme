<?php

include("./php/db.php");
$Database = new Database();


// Login überprüfen
if(isset($_POST['login-submit'])) {
	// Login-Form wurde abgesendet
	$username = $_POST['username'];
	$password = $_POST['password'];

	//Aus Usernamen folgendes entfernen
	//leerzeiche,tabulatorzeiche,Zeilenvorschub,Wagenrücklaufzeiche,NUL-Byte, vertikaler Tabulatur
    $username = trim ( $username, $character_mask = " \t\n\r\0\x0B");

	// Einloggen
	if($Database->login($username, $password)) {
		echo "<div class='alert alert-success'>Login erfolgreich!</div>";
	}
	else {
		echo "<div class='alert alert-danger'>Login fehlgeschlagen!</div>";
	}
}


// Registrierung überprüfen
if(isset($_POST['register-submit'])) {
	// Registrierungs-Form wurde abgesendet
	$username = $_POST['username'];
	$email = $_POST['email'];
	$password = $_POST['password'];
	$confirm_password = $_POST['confirm-password'];

    //Aus Usernamen folgendes entfernen
    //leerzeiche,tabulatorzeiche,Zeilenvorschub,Wagenrücklaufzeiche,NUL-Byte, vertikaler Tabulatur
	$username = trim ( $username, $character_mask = " \t\n\r\0\x0B");

	// Passwörter überprüfen
	if($password != $confirm_password) {
		echo "<div class='alert alert-danger'>Registrierung fehlgeschlagen! Passwörter stimmen nicht überein!</div>";
	}
	else {
		// Benutzer-Name überprüfen (ob vorhanden)
		if($Database->userExists($username, $email)) {
			echo "<div class='alert alert-danger'>Registrierung fehlgeschlagen! E-Mail-Adresse ist schon vergeben!</div>";
		}
		else {
			// Registrieren
			if($Database->register($username, $email, $password)) {
				echo "<div class='alert alert-success'>Registrierung erfolgreich!</div>";
			}
			else {
				echo "<div class='alert alert-danger'>Registrierung fehlgeschlagen!</div>";
			}
		}
	}
}


// Link prüfen und falls vorhanden speichern
if(isset($_GET['link'])) {
	$_SESSION['link'] = $_GET['link'];
	echo "<script>var linkResult = 'Bitte melde dich an, damit der Link verarbeitet werden kann!';</script>";
}



// prüfen, ob User eingeloggt ist
if($Database->isLoggedIn()) {
    // zur Chat-Seite weiterleiten
    header('Location: ./chat');
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <!--Browser / Tab - Title einstellen-->
        <title>TextMe - Login/Registrierung</title>

        <!--Meta Informationen der Website-->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!--Stylesheets einbinden-->
        <link rel="stylesheet" type="text/css" href="./css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="./css/login-signup.css">
        <link rel="stylesheet" type="text/css" href="./css/cookie.css" />

        <!--Icon "einrichten"-->
        <link rel="icon" href="./images/Speach-BubbleDialog-512.png">

    </head>
    <body>
    <!-- noscript Funktion -
    Fängt User ab die JavaScript deaktivert haben
    weißt auf eine Anleitung hin um JavaScript wieder zu aktivieren
    -->
    <noscript>
        <div id="nojavascript">
            Diese Anwendung benötitgt JavaScript zum ordungsgemäßen Betrieb.
            Bitte <a href="https://www.enable-javascript.com/" target="_blank" rel="noreferrer"> aktivieren Sie Java Script</a>
            und laden Sie die Seite neu.
        </div>
    </noscript>


    <div class="container">
        <!-- Pineapple Logo -->
        <div style="width: 100%">
            <div class="login-width">
                <img src="images/logo2.png" style="width: 30%; height: 30%">
            </div>
        </div>
        <!-- Login / Registierungs Bereich -->
        <div style="width: 100%">
            <div class="login-width">
                <div class="panel panel-login">
                    <!-- Überschriften (Login / Registierun)-->
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-sm-6">
                                <a href="#" class="active" id="login-form-link">Login</a>
                            </div>
                            <div class="col-sm-6">
                                <a href="#" id="register-form-link">Registrieren</a>
                            </div>
                        </div>
                        <hr>
                    </div>

                    <!-- Body - Input Felder / Buttons-->
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- Login Bereich -->
                                <form id="login-form" action="index.php" method="post" role="form" style="display: block;">
                                    <!-- Input Feld für Username -->
                                    <div class="form-group">
                                        <input type="text" name="username" id="login-username" tabindex="1" class="form-control" placeholder="Username" value="">
                                    </div>
                                    <!-- Fehlermeldung - ausgeblendet -->
                                    <div class="alert alert-danger" role="alert" style=" display:none; text-align: left" id="username-error">mindestens drei Zeichen!</div>
                                    <!-- Input Feld Password -->
                                    <div class="form-group">
                                        <input type="password" name="password" id="login-password" tabindex="2" class="form-control" placeholder="Password">
                                    </div>
                                    <!-- Fehlermeldung - ausgeblendet -->
                                    <div class="alert alert-danger" role="alert" style=" display:none; text-align: left" id="password-error">mindestens sechs Zeichen!</div>
                                    <!-- Login Button-->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-6 col-sm-offset-3">
                                                <input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Log In">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Bereich unter den Eingabe Feldern-->
                                    <!-- Link zur Seite Password Vergessen seite -->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="text-center">
                                                    <a href="php/passwordReset.php" tabindex="5" class="forgot-password">Passwort vergessen?</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                                <!-- Registerungs bereich -->
                                <form id="register-form" action="" method="post" role="form" style="display: none;">
                                    <!-- Input Username-->
                                    <div class="form-group">
                                        <input type="text" name="username" id="register-username" tabindex="1" class="form-control" placeholder="Username" value="">
                                    </div>
                                    <!-- Fehlermeldung - ausgeblendet -->
                                    <div class="alert alert-danger" role="alert" style=" display:none; text-align: left" id="register-username-error">mindestens drei Zeichen!</div>
                                    <!-- Input Email adresse -->
                                    <div class="form-group">
                                        <input type="email" name="email" id="email" tabindex="1" class="form-control" placeholder="E-Mail-Adresse" value="">
                                    </div>
                                    <!-- Fehlermeldung - ausgeblendet -->
                                    <div class="alert alert-danger" role="alert" style=" display:none; text-align: left" id="register-email-error">ungültige E-Mail-Adresse!</div>
                                    <!-- Input Password-->
                                    <div class="form-group">
                                        <input type="password" name="password" id="register-password" tabindex="2" class="form-control" placeholder="Password">
                                    </div>
                                    <!-- Fehlermeldung - ausgeblendet -->
                                    <div class="alert alert-danger" role="alert" style=" display:none; text-align: left" id="register-password-error">minimum six characters!</div>
                                    <!-- Input passwort 2 - zur Überprüfung -->
                                    <div class="form-group">
                                        <input type="password" name="confirm-password" id="confirm-password" tabindex="2" class="form-control" placeholder="bestätige dein Passwort">
                                    </div>
                                    <!-- Fehlermeldung ausgebelndet -->
                                    <div class="alert alert-danger" role="alert" style=" display:none; text-align: left" id="register-password-confirm-error">Passwörter stimmen nicht überein!</div>
                                    <!-- Register Button -->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-6 col-sm-offset-3">
                                                <input type="submit" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Registrieren">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Cookie Popup -->
    <script src="./js/cookie.js"></script>
    <script>
        window.addEventListener("load", function() {
            window.cookieconsent.initialise({
                "palette": {
                    "popup": {
                        "background": "#edeff5",
                        "text": "#838391"
                    },
                    "button": {
                        "background": "transparent",
                        "text": "#4b81e8",
                        "border": "#4b81e8"
                    }
                },
                "content": {
                    "message": "Diese Website verwendet Cookies. Informationen zu Cookies und ihrer Deaktivierung finden Sie ",
                    "dismiss": "Verstanden und weiter",
                    "link": "hier.",
                }
            });

            if (typeof linkResult !== 'undefined') {
                if(linkResult != null) {
                    $("body").prepend("<div class=\"alert alert-danger\" role=\"alert\">"+linkResult+"</div>")
            	    //alert(linkResult);
                }
            }
        });
    </script>

    <!-- Java Script Dateien einbinden -->
    <script src="js/vendor/jquery-3.3.1.js"></script>
    <script src="js/vendor/bootstrap-4.0.0-dist/js/bootstrap.js"></script>
    <script src="js/login-signup.js"></script>
    </body>
</html>