$(function() {

    // wenn button Login geklickt wird zeigt er die Felder vom Login an und die von der Registierung aus
    $('#login-form-link').click(function(e) {
        $("#login-form").delay(100).fadeIn(100);
        $("#register-form").fadeOut(100);
        $('#register-form-link').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
    });

    // wenn button Registieren geklickt wird zeigt er die Felder vom Registierung an und die von der Login aus
    $('#register-form-link').click(function(e) {
        $("#register-form").delay(100).fadeIn(100);
        $("#login-form").fadeOut(100);
        $('#login-form-link').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
    });

    //ruft Funktion Login Überprüfen und Registration Überprüfen auf
    validateLogin();
    validateRegistration();

});

/**
 * validates username and password
 * */
function validateLogin() {

    //Felder mit der den entsprechenden ID den entsprechenden Variabeln zuweisen
    const loginUsernameField = $("#login-username");
    const loginPasswordField = $("#login-password");

    //wenn ich auf login Button drücke
    $('.btn-login').click(function (e) {
        //speichert username und passwort in Variabel
        const username = loginUsernameField.val();
        const password = loginPasswordField.val();

        //wenn Username kürzer als 3 Zeichen ist --> Fehler anzeigen
        if(username.length < 3) {
            e.preventDefault();
            //console.log("username error");
            loginUsernameField.addClass("invalid");
            $("#username-error").show();
        }
        //wenn Password kürzer als 6 Zeichen ist --> Fehler anzeigen
        if(password.length < 6){
            e.preventDefault();
            //console.log("password error");
            loginPasswordField.addClass("invalid");
            $("#password-error").show();
        }
    });

    //wenn ich etwas in das Feld username eingebe und vorher ein Fehler angezeigt wurde wird der jetzt ausgeblendet
    loginUsernameField.keypress(function () {
        if(loginUsernameField.hasClass("invalid")) {
            loginUsernameField.removeClass("invalid");
            $("#username-error").hide();
        }
    });

    //wenn ich etwas in das Feld Password eingebe und vorher ein Fehler angezeigt wurde wird der jetzt ausgeblendet
    loginPasswordField.keypress(function () {
        if(loginPasswordField.hasClass("invalid")) {
            loginPasswordField.removeClass("invalid");
            $("#password-error").hide();
        }
    });
}

/**
 * validates every input field
 */
function validateRegistration() {

    //Felder mit der den entsprechenden ID den entsprechenden Variabeln zuweisen
    const registerUsernameField = $("#register-username");
    const registerPasswordField = $("#register-password");
    const registerEmailField = $("#email");
    const registerPasswordConfirmField = $("#confirm-password");

    //wenn ich auf Registeren Button drücke
    $('.btn-register').click(function (e) {
        //speichert username, passwort, passwordConfirm und Email in Variabel
        const username = registerUsernameField.val();
        const password = registerPasswordField.val();
        const passwordConfirm = registerPasswordConfirmField.val();
        const email  = registerEmailField.val();

        //wenn Username kürzer als 3 Zeichen ist --> Fehler anzeigen
        if(username.length < 3) {
            e.preventDefault();
//            console.log("username error");
            registerUsernameField.addClass("invalid");
            $("#register-username-error").show();
        }

        //Gibt Fehlermeldung aus wenn Funktion ein False zurück liefert - Funktion prüft Email adresse auf Gültigkeit
        if(!isValidEmailAddress(email)) {
            e.preventDefault();
//            console.log("email error");
            registerEmailField.addClass("invalid");
            $("#register-email-error").show();
        }

        //Funktion Prüft das Password und gibt entsprechende Fehlermeldung zurück - diese werden in Variabel gespeichert
        const passwordMessage = validatePassword(password);

        //wenn diese Variabel nicht leer ist, dann gibt er diese Fehlermeldungen aus
        if(passwordMessage !== ""){
            e.preventDefault();
//            console.log(passwordMessage);
            registerPasswordField.addClass("invalid");
            $("#register-password-error").html(passwordMessage);
            $("#register-password-error").show();
        }

        //wenn passwort Feld 1 und 2 nicht übereinstimmen gibt er fehler aus
        if(passwordConfirm !== password){
            e.preventDefault();
//            console.log("password confirm error");
            registerPasswordConfirmField.addClass("invalid");
            $("#register-password-confirm-error").show();
        }
    });

    //wenn ich etwas in das Feld Username eingebe und vorher ein Fehler angezeigt wurde wird der jetzt ausgeblendet
    registerUsernameField.keypress(function () {
        if(registerUsernameField.hasClass("invalid")) {
            registerUsernameField.removeClass("invalid");
            $("#register-username-error").hide();
        }
    });

    //wenn ich etwas in das Feld E-Mail eingebe und vorher ein Fehler angezeigt wurde wird der jetzt ausgeblendet
    registerEmailField.keypress(function () {
        if(registerEmailField.hasClass("invalid")) {
            registerEmailField.removeClass("invalid");
            $("#register-email-error").hide();
        }
    });

    //wenn ich etwas in das Feld Password eingebe und vorher ein Fehler angezeigt wurde wird der jetzt ausgeblendet
    registerPasswordField.keypress(function () {
        if(registerPasswordField.hasClass("invalid")) {
            registerPasswordField.removeClass("invalid");
            $("#register-password-error").hide();
        }
    });

    //wenn ich etwas in das Feld PasswordConfirm eingebe und vorher ein Fehler angezeigt wurde wird der jetzt ausgeblendet
    registerPasswordConfirmField.keypress(function () {
        if(registerPasswordConfirmField.hasClass("invalid")) {
            registerPasswordConfirmField.removeClass("invalid");
            $("#register-password-confirm-error").hide();
        }
    });


    /**
     * Prüft E-Mail-Adresse auf Gültigkeit
     * @param emailAddress
     * @returns {boolean}
     */
    function isValidEmailAddress(emailAddress) {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(emailAddress);
    }

    /**
     * Validiert das übergebene Passwort
     * @param str
     * @returns {string}
     */
    function validatePassword(str) {
        let returnMessage = "";
        if (str.length < 6) {
            returnMessage = returnMessage.concat("<li>Muss mindestens 6 Zeichen lang sein</li><br>");
        } if (str.search(/\d/) == -1) {
            returnMessage = returnMessage.concat("<li>muss mindestens 1 Ziffer enthalten</li><br>");
        } if (str.search(/[A-Z]/) == -1) {
            returnMessage = returnMessage.concat("<li>muss mindestens 1 Großbuchstaben enthalten</li><br>");
        } if (str.search(/[a-z]/) == -1) {
            returnMessage = returnMessage.concat("<li>muss mindestens 1 Kleinbuchstabe enthalten</li><br>");
        } if (str.search(/[!"#\$%&'\(\)\*\+,\-\.\/:;<=>\?@\[\]\^_`{\|}~]/) == -1) {
            returnMessage = returnMessage.concat("<li>muss mindestens 1 Sonderzeichen enthalten</li>");
        }
        return(returnMessage);
    }
}